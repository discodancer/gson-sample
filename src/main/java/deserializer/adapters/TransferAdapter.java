package deserializer.adapters;

import com.google.gson.*;
import deserializer.GsonDeserializer;
import models.Person;
import models.Transfer;

import java.lang.reflect.Type;

public class TransferAdapter implements JsonDeserializer<Transfer> {

    @Override
    public Transfer deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Transfer model = new Gson().fromJson(json, Transfer.class);
        JsonObject jsonObject = json.getAsJsonObject();

        if (jsonObject.has("person")) {
            JsonElement element = jsonObject.get("person");
            if (element.isJsonObject() && !element.isJsonNull() && element != null) {
                String valuesString = element.toString();
                Person person = GsonDeserializer.deserialize(
                        valuesString,
                        Person.class,
                        new PersonAdapter()
                );
                model.setPerson(person);
            }
        }

        return model;
    }
}