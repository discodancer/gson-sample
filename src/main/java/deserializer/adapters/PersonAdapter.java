package deserializer.adapters;

import com.google.gson.*;
import deserializer.GsonDeserializer;
import models.Country;
import models.Person;

import java.lang.reflect.Type;

public class PersonAdapter implements JsonDeserializer<Person> {

    @Override
    public Person deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Person model = new Gson().fromJson(json, Person.class);
        JsonObject jsonObject = json.getAsJsonObject();

        if (jsonObject.has("citizenship")) {
            JsonElement element = jsonObject.get("citizenship");
            if (element.isJsonObject() && !element.isJsonNull() && element != null) {
                String valuesString = element.toString();
                Country value = GsonDeserializer.deserialize(valuesString, Country.class);
                model.setCitizenship(value);
            } else {
                model.setCitizenship(element.getAsString());
            }
        }

        return model;
    }
}
