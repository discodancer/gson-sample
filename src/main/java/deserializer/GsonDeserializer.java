package deserializer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;
import lombok.Setter;

import java.lang.reflect.Type;

@Setter
public class GsonDeserializer {

    public static Double version;

    public static <T> T deserialize(String json, Type type) {
        Gson mapper = new GsonBuilder()
                .setVersion(version)
                .create();

        return mapper.fromJson(json, type);
    }

    public static <T> T deserialize(String json, Type type, JsonDeserializer deserializer) {
        Gson mapper = new GsonBuilder()
                .setVersion(version)
                .registerTypeAdapter(type, deserializer)
                .create();

        return mapper.fromJson(json, type);
    }

    public static String serialize(Object model) {
        Gson mapper = new GsonBuilder()
                .setPrettyPrinting()
                .create();

        return mapper.toJson(model);
    }
}
