import deserializer.GsonDeserializer;
import deserializer.adapters.TransferAdapter;
import models.Transfer;
import utils.ResourcesLoader;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        String transferJson;
        Transfer transferModel;

        GsonDeserializer.version = 2.2;
        transferJson = ResourcesLoader.getResourceAsString("/transfer_v2.2.json");
        transferModel = GsonDeserializer.deserialize(transferJson, Transfer.class, new TransferAdapter());
        System.out.println("\nTransfer v2.2:\n" + GsonDeserializer.serialize(transferModel));

        GsonDeserializer.version = 2.3;
        transferJson = ResourcesLoader.getResourceAsString("/transfer_v2.3.json");
        transferModel = GsonDeserializer.deserialize(transferJson, Transfer.class, new TransferAdapter());
        System.out.println("\n\n\nTransfer v2.3:\n" + GsonDeserializer.serialize(transferModel));
    }
}
