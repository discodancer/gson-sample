package models;

import com.google.gson.annotations.Since;
import com.google.gson.annotations.Until;
import lombok.Data;

@Data
public class Country {

    @Since(2.2)
    private String name;

    @Since(2.2)
    @Until(2.3)
    private String phonePrefix;

    @Since(2.2)
    @Until(2.3)
    private String extendedPhonePrefix;

    @Since(2.2)
    @Until(2.3)
    private String phoneMinLength;

    @Since(2.2)
    @Until(2.3)
    private String phoneMaxLength;

    @Since(2.2)
    @Until(2.3)
    private String phoneFormat;

    @Since(2.3)
    private PhoneInfo phoneInfo;
}
