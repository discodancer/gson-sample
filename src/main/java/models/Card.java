package models;

import com.google.gson.annotations.Since;
import com.google.gson.annotations.Until;
import lombok.Data;

@Data
public class Card {

    @Since(2.2)
    private String name;

    @Since(3.0)
    private String overField;

    @Until(1.0)
    private String nearField;
}
