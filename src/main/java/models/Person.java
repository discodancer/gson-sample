package models;

import com.google.gson.annotations.Since;
import lombok.Data;

@Data
public class Person<T> {

    @Since(2.2)
    private String name;

    @Since(2.2)
    private T citizenship;
}
