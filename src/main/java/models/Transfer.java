package models;

import com.google.gson.annotations.Since;
import lombok.Data;

@Data
public class Transfer {

    @Since(2.2)
    private String name;

    @Since(2.2)
    private Person person;

    @Since(2.2)
    private Country country;

    @Since(2.2)
    private Currency currency;

    @Since(2.2)
    private Card card;

    @Since(2.3)
    private String cardDataSource;
}
