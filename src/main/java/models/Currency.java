package models;

import com.google.gson.annotations.Since;
import lombok.Data;

@Data
public class Currency {

    @Since(2.2)
    private String name;
}
